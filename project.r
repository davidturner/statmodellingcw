# functions
getmode <- function(v) {
   uniqv <- unique(v)
   uniqv[which.max(tabulate(match(v, uniqv)))]
}

data = read.csv('data/cheese-sales-record.csv')
names(data) <- c('units_sold')

hist(data$units_sold,
    main='Units of cheese sold in a day',
    xlab='Units Sold',
    ylab='Frequency'
)

u <- data$units_sold

u.mean <- mean(u, na.rm=FALSE)
u.median <- median(u)
u.mode <- getmode(u)
u.sd <- sd(u)
u.var <- var(u)
u.iqr <- IQR(u)


library(MASS)
rnegbin(10, 2, 10)
